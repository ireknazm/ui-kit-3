// Just for alias support in WebStorm IDE
/* eslint-disable */
import path from 'path';

export default {
  resolve: {
    // for WebStorm
    alias: {
      '@mp/ui-kit': path.resolve(__dirname, 'src/components'),
      styles: path.join(__dirname, 'src/styles'),
      assets: path.join(__dirname, 'src/assets'),
    }
  }
};
