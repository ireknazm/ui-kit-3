import Vue from 'vue';

import Tab from "../src/components/Tab/Tab.vue";


export default {
  title: 'Tab'
}

export const Default = () => ({
  components: { Tab },
  template: `
  <div style="background: black"><Tab>Ama default tab</Tab></div>
  `
});

export const ActiveTab = () => ({
  components: { Tab },
  template: `
    <div style="display:flex; justify-content: center; background: black"><Tab active>Ama acitve beach</Tab></div>
  `
});
