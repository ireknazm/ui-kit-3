import Vue from 'vue';

import Logo from '../src/components/Logo/Logo.vue';

export default {
  title: 'Logo'
}

export const Default = () => ({
  components: { Logo },
  template: `
  <div><Logo /></div>
  `
});