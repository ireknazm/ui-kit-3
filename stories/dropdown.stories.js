import Vue from 'vue';
import Dropdown from '../src/components/Dropdown/Dropdown.vue';

export default { title: 'Dropdown' };

Vue.component('ui-dropdown', Dropdown);

export const Default = () => '<ui-dropdown>Default dropdown</ui-dropdown>';

export const Styled = () => ({
  components: { Dropdown },

  data() {
    return {
      items: [
        {value: 1, text: 'First element'},
        {value: 2, text: 'Second element'},
        {value: 3, text: 'Third element'}
      ],
      active: {value: 1, text: 'First element'},
      items2: [
        {value: 1, text: 'Jan'},
        {value: 2, text: 'Feb'},
        {value: 3, text: 'March'}
      ],
      active2: {value: 2, text: 'Feb'}
    };
  },
  template: `
    <div class="container">
      <div class="mb-4 row">
        <div class="col col-xs-4">
          <ui-dropdown v-model="active" :items="items" :showChevron="true"/>
        </div>
        <div class="col col-xs-4">
          <ui-dropdown v-model="active" :items="items">
            <template slot="trigger">
              <p>{{active.text}}</p>
            </template>
          </ui-dropdown>
        </div>
        <div class="col col-xs-4">
          <ui-dropdown>
            <template slot="trigger">
              <p>Custom content</p>
            </template>
            <template slot="content">
              <div class="container">
                <div class="row">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium cupiditate distinctio, enim facilis inventore, nisi obcaecati officia possimus provident quas rem saepe sed sequi, soluta vel vero. Autem, est.</p>
                </div>
              </div>
            </template>
          </ui-dropdown>
        </div>
        <div class="col col-xs-4">
          <ui-dropdown v-model="active2" :items="items2" :showChevron="true">
            <template slot="trigger">
              <p>{{active2.text}}</p>
            </template>
          </ui-dropdown>
        </div>
      </div>
    </div>
  `
});
