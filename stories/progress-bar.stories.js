import Vue from 'vue';
import ProgressBar from '../src/components/ProgressBar/ProgressBar';

export default { title: 'Progress Bar' };

Vue.component('ui-progress-bar', ProgressBar);

export const Default = () => '<ui-progress-bar :value="20">Progress bar</ui-progress-bar>';

export const Styled = () => ({
  components: { ProgressBar },
  data() {
    return {
      width: 0,
      timer: ""
    };
  },
  template: `
    <div class="container">
      <div class="mb-4 row">
        <ui-button size="md" primary type="button" @click="loading">
          Start the loading
        </ui-button>
      </div>
      
      <div class="row mb-4">
        <ui-progress-bar v-model="width" variant="danger" :height="18">
          <template slot="value">
            Loaded
            <strong class="custom-text">{{ width.toFixed(0) }}%</strong>
          </template>
        </ui-progress-bar>
      </div>

      <div class="row mb-4">
        <ui-progress-bar :value="width" :height="4">
          <template slot="helper">
            <span>Loading...</span>
          </template>
        </ui-progress-bar>
      </div>
      
      <div class="mb-4 row">
        <ui-progress-bar :value="50" variant="warning" :height="3">
          <template slot="label">
            <span>Please, wait...</span>
          </template>
        </ui-progress-bar>
      </div>

      <div class="mb-4 row">
        <ui-progress-bar :value="75" variant="dark">
          <template slot="helper">
            <span>Loading...</span>
          </template>
        </ui-progress-bar>
      </div>

      <div class="row">
        <ui-progress-bar
            :value="100"
        />
      </div>
    </div>
  `,
  methods: {
    clearTimer() {
      if (this.timer) {
        clearInterval(this.timer);
        this.timer = null;
      }
    },
    loading: function() {
      if (this.width !== 1) {
        this.width = 1;
      }
      this.timer = setInterval(() => {
        this.width += Math.floor(Math.random() * 2);
        if (this.width > 98) {
          this.width = 100;
          this.clearTimer();
        }
      }, Math.floor(60 * (Math.random() * 2)));
    }
  }
});
