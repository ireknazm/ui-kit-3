import Vue from "vue";
import Breadcrumb from "../src/components/Breadcrumb/Breadcrumb.vue";
import BreadcrumbItem from "../src/components/Breadcrumb/BreadcrumbItem.vue";

export default { title: "Breadcrumb" };

Vue.component("breadcrumb", Breadcrumb);

export const Styled = () => ({
  components: { Breadcrumb, BreadcrumbItem },
  data() {
    return {
      loading: false
    };
  },
  methods: {
    onClick() {
      alert('Cart');
    }
  },
  template: `
    <div>
      <Breadcrumb>
        <BreadcrumbItem href="#">Главная</BreadcrumbItem>
        <BreadcrumbItem href="#">Реклама на транспорте</BreadcrumbItem>
        <BreadcrumbItem href="#" :on-click="onClick">Корзина</BreadcrumbItem>
      </Breadcrumb>
    </div>
  `
});
